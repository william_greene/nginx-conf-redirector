#******************************************************************
#**
#**  nginx-conf master build script
#**
#******************************************************************
#** 
#** Copyright (C) 2012 Snowball Factory Inc.
#** 
#******************************************************************

# we need cmake 2.8.3
cmake_minimum_required(VERSION 2.8.3)

# setup our package info
SET(ANTIPASTI_NAME "nginx-conf-redirector")
SET(ANTIPASTI_PROVIDES "nginx-conf")
SET(ANTIPASTI_REPLACES "nginx-conf, nginx")
SET(ANTIPASTI_CONFLICTS "nginx-conf, apache2")
SET(ANTIPASTI_ARCH_ALL 1)
SET(ANTIPASTI_DEPENDS "nginx, coreutils, sysv-rc, lsb-base, initscripts")
SET(ANTIPASTI_CONTROL_FILES "postinst" "prerm" "conffiles")
SET(ANTIPASTI_CONTACT "Awe.sm Packages <packages@awe.sm>")
SET(ANTIPASTI_DESCRIPTION "nginx configuration for redirectors
 Nginx configuration for redirectors.")

# setup our files to install
SET(CMAKE_INSTALL_PREFIX "/")
INSTALL(FILES
	nginx.conf
	DESTINATION "etc/nginx")
INSTALL(FILES
	php-redirector
	DESTINATION "etc/nginx/sites-available")
INSTALL(FILES
	copyright
	DESTINATION "usr/local/share/doc/${ANTIPASTI_NAME}")

# include antipasti
INCLUDE(Antipasti)

